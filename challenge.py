from constant import URL_HUB, AUTHORIZATION, CAMINHO_CERTIFICADO, SENHA_CERTIFICADO, MODE, HASH_ALGORITHM
from OpenSSL import crypto
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA512, SHA256, SHA
from base64 import b64encode, b64decode
import requests, random, string, json

#Define o HEADER da requisição e a URL que será utilizada
HEADER = { 'Authorization': AUTHORIZATION, 'Content-Type': 'application/json'}
URL_DESAFIO = URL_HUB + '/api/authentication-service/v1/verify-challenge'

#Verifica se o token foi inicializado
def token_verify():
    if (len(AUTHORIZATION) == 0 or AUTHORIZATION == 'insert-a-valid-token'):
        print('Necessário configurar um token de acesso')
        return False

    return True

#Gera uma String aleatória em base64 para ser assinada (Desafio)
def gerarDesafio():
    stringAleatoria = ''.join(random.choice(string.ascii_letters) for i in range(64))
    
    return b64encode(stringAleatoria.encode('utf-8')).decode('utf-8')

#Assina com certificado armazenado em disco e retorna o valor da assinatura como uma string base64
def criptografaConteudo(conteudo, chavePrivada):
    print("================  Inicializando assinatura  ================")
    print("")
    conteudo = b64decode(conteudo)

    key = crypto.dump_privatekey(crypto.FILETYPE_PEM, chavePrivada).decode("utf-8")

    rsa_key = RSA.importKey(key)

    if HASH_ALGORITHM == 'SHA1':
        hash_object = SHA.new(conteudo)
    elif HASH_ALGORITHM == 'SHA256':
        hash_object = SHA256.new(conteudo)
    elif HASH_ALGORITHM == 'SHA512':
        hash_object = SHA512.new(conteudo)
    else:
        print("Algoritmo de HASH inválido. Valores aceitos: 'SHA1, SHA256, SHA512' ")

    
    signature = PKCS1_v1_5.new(rsa_key).sign(hash_object)

    print("Valor da assinatura em Base64 ", b64encode(signature).decode('utf-8'))
    print("")

    return b64encode(signature).decode('utf-8')

#Verifica desafio
def verificarDesafio(certificate, challenge, challengeSignature):
    #Json com os dados da requisição
    inicialization_form = '{ "certificate" : "' + certificate + '", "mode" : "' + MODE + '", "hashAlgorithm" : "' + HASH_ALGORITHM + '","challenge" : "' + challenge + '","challengeSignature" : "' + challengeSignature + '"}'

    print('============= Inicializando Verificação de Desafio no BRY HUB =============')
    print("")
    print('Desafio: ' + challenge)

    response = requests.post(URL_DESAFIO, data = inicialization_form, headers=HEADER)
    
    if response.status_code == 200:
        print("")
        print('Desafio verificado com sucesso.')
        data = response.json()
        statusCertificado = data['certificateReport']['status']['status']
        statusAssinatura = data['signatureVerified']


        print('O certificado do assinante está com status:', statusCertificado)
        print('A assinatura do desafio esta com valor:', statusAssinatura)
        print("")
        if (statusCertificado == 'VALID' and statusAssinatura == 'true'):
            return True
        
    else:
        print(response.text)
    
    return False

if(token_verify()):
    #Passo 1:
    #Gera o desafio que será assinado pelo certificado. Este passo deve ser realizado pelo servidor de autenticação.
    desafio = gerarDesafio()

    #Passo 2:
    #Carrega a chave privado do certificado que será utilizado para assinatura
    p12 = crypto.load_pkcs12(open(CAMINHO_CERTIFICADO, 'rb').read(), SENHA_CERTIFICADO)
    privateKey = p12.get_privatekey()
    #Carrega a chave publica do certificado
    cert = p12.get_certificate()
    pem = crypto.dump_certificate(crypto.FILETYPE_PEM, cert).decode('utf-8').replace('-----BEGIN CERTIFICATE-----', '').replace('-----END CERTIFICATE-----', '').replace('\n', '')

    #Passo 3:
    #Assina o desafio utilizando o certificado
    signature = criptografaConteudo(desafio, privateKey)

    #Passo 4:
    #Realiza a requisição para o HUB para validação da assinatura e certificado, verificando se o desafio foi aceito. Este passo deve ser realizado pelo servidor de autenticação.
    if (verificarDesafio(pem, desafio, signature)):
        print('O usuário foi autenticado com sucesso!')
    else:
        print('O usuário não pode ser autenticado')